// Code pour définir l'application comme pouvant recevoir des SMS..On utilisera pour ce fait les BroadcastReceiver
package io.insti.lok.sms_app;



import android.content.BroadcastReceiver;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.content.Intent;
import android.telephony.SmsMessage;
import android.util.Log;
import android.widget.Toast;


public class MonReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        //Stocker le message dans le bundle sous forme de PDUs
            Bundle bundle = intent.getExtras();
            if (bundle != null) {
                try {
                    // Récupérer le/les PDUs en utilisant la clef "pdus" Sur le Bundle
                    Object[] pdus = (Object[]) bundle.get("pdus");
                    String  smsMessageStr="";
                    // Récupérer le tableau d'Object retourné par le Bundle
                    SmsMessage[] messages = new SmsMessage[pdus.length];
                    for (int i = 0; i < pdus.length; i++) {
                        //Convertir les PDUSs en messages
                        messages[i] = SmsMessage.createFromPdu((byte[]) pdus[i]);
                    }
                    // Enfin traiter les messages
                    for (SmsMessage message : messages) {
                        String from = message.getOriginatingAddress(); //On récupere Le numéro de l'emetteur du SMS
                        String msg = message.getMessageBody(); // le corps du message
                        long when = message.getTimestampMillis(); // la date et l'heure de l'envoi
                        smsMessageStr += "SMS From :" + from + "\n";
                        smsMessageStr += msg + "\n";
                    }
                    Toast.makeText(context, smsMessageStr, Toast.LENGTH_SHORT).show();
                    ReceiverActivity inst = ReceiverActivity.instance();
                    inst.updateList(smsMessageStr);
                } catch (Exception e) {
                    Log.d("Exception caught", e.getMessage());
                }
            }
        }


            }




