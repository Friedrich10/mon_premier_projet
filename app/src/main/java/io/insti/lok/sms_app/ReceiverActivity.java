package io.insti.lok.sms_app;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.content.ContentResolver;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

public class ReceiverActivity extends AppCompatActivity {
private static ReceiverActivity inst;
ArrayList<String> smsMessagesList = new ArrayList<String>();
ListView smsListView;
ArrayAdapter arrayAdapter;
public static ReceiverActivity instance() {return inst;}
@Override
public void onStart(){
    super.onStart();
    inst=this;
}
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_receiver);
        smsListView = (ListView) findViewById(R.id.SMSList);
        arrayAdapter= new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, smsMessagesList);
        smsListView.setAdapter(arrayAdapter);
        //smsListView.setOnClickListener(this);
        // Add SMS Read Permission At Runtime
        // Todo : If Permission is Not Granted
        if (ContextCompat.checkSelfPermission(getBaseContext(), "android.permission.READ_SMS") == PackageManager.PERMISSION_GRANTED){
            refreshSmsInbox();
        }else{
            // Todo : Then Set Permission
            final int REQUEST_CODE_ASK_PERMISSIONS = 123;
            ActivityCompat.requestPermissions(ReceiverActivity.this, new String[]{"android.permission.READ_SMS"}, REQUEST_CODE_ASK_PERMISSIONS);
        }
}
public void refreshSmsInbox() {
    ContentResolver contentResolver = getContentResolver();
    Cursor smsInboxCursor = contentResolver.query(Uri.parse("content://sms/"), null, null, null, null);
    int indexBody = smsInboxCursor.getColumnIndex("body");
    int indexAddress = smsInboxCursor.getColumnIndex("address");
    if (indexBody < 0 || !smsInboxCursor.moveToFirst()) return;
    arrayAdapter.clear();
    do {
        String str = smsInboxCursor.getString(indexAddress) + "\n" + smsInboxCursor.getString(indexBody) + "\n";
        arrayAdapter.add(str);
    }while (smsInboxCursor.moveToNext());
}
public void updateList(final String smsMessage){
    arrayAdapter.insert(smsMessage, 0);
    arrayAdapter.notifyDataSetChanged();
}
public void onIemClick(AdapterView<?> parent, View view, int pos, long id){
    try {
        String[] smsMessages = smsMessagesList.get(pos).split("\n");
        String address = smsMessages[0];
        String smsMessage = "";
        for (int i = 1; i< smsMessages.length; ++i){
            smsMessage += smsMessages[i];
        }
        String smsMessageStr = address + "\n";
        smsMessageStr += smsMessage;
        Toast.makeText(this, smsMessageStr, Toast.LENGTH_SHORT).show();
    }catch (Exception e) {
        e.printStackTrace();
    }
}
}