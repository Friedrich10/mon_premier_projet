/*
Code java pour mettre en place le mécanisme d'envoi de SMS
 */
package io.insti.lok.sms_app;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import android.app.Notification;
import android.Manifest;
import android.app.NotificationManager;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.util.Log;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private static final int MY_PERMISSION_REQUEST_CODE_SEND_SMS=1;
    private static final String LOG_TAG = "AndroidExample";
Button btn;
EditText phone;
EditText sms;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        // Récupération de nos composants.
        phone = findViewById(R.id.nume);
        btn = findViewById(R.id.btn_send);
        sms = findViewById(R.id.smsText);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                askPermissionAndSendSMS();
            }
        });
    }
    private void askPermissionAndSendSMS() {
        // Avec niveau Android >= 23, on demande à l'utilisateur l'autorisation d'envoyer des SMS
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            //On vérifie si l'on a la permission d'envoyer des SMS
            int sendSmsPermission = ActivityCompat.checkSelfPermission(this, Manifest.permission.SEND_SMS);
            if (sendSmsPermission != PackageManager.PERMISSION_GRANTED) {
                //Si l'on n'a pas la permission, on la demande à l'utilisateur
                this.requestPermissions(new String[]{Manifest.permission.SEND_SMS}, MY_PERMISSION_REQUEST_CODE_SEND_SMS);
                return;
            }
        }
        this.sendSMS_by_smsManager();
    }
    private void sendSMS_by_smsManager(){
             String phoneNum = phone.getText().toString();
             String smsMess = sms.getText().toString();
             try {
                 //Grâce à la méthode de gestion de SMS(SmsManager) que l'on récupère via la méthode static getDefault()
                 SmsManager smsManager = SmsManager.getDefault();
                 //On envoie le SMS à l'aide de la méthode sendTextMessage
                 smsManager.sendTextMessage(phoneNum, null, smsMess, null, null);
                 phone.setText("");
                 sms.setText("");
                 //Affiche d'un petit message dans un toast
                 Log.i(LOG_TAG,"Votre SMS a été bien envoyé !");
                 Toast.makeText(getApplicationContext(), "SMS envoyé !", Toast.LENGTH_LONG).show();
             }catch (Exception e){
                 Log.e(LOG_TAG,"L'envoi de votre SMS a échoué", e);
                 Toast.makeText(getApplicationContext(), "Echec d'envoi! Veuillez réessayer", Toast.LENGTH_LONG).show();
                 e.printStackTrace();
             }
         }
// Lorsqu'on a les résultats de la demande
    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults){
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode){
            case MY_PERMISSION_REQUEST_CODE_SEND_SMS:{
                // Si la demande est annulée, les tableaux de résultats sont vides.
                // Autorisatisations accordées(envoyer_SMS)
                if (grantResults.length>0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                    Log.i(LOG_TAG, "Permission granted");
                    Toast.makeText(this, "Permission granted!", Toast.LENGTH_LONG).show();
                    this.sendSMS_by_smsManager();
                }
                // Annulée ou rejetée
                else{
                    Log.i(LOG_TAG, "Permission denied!");
                    Toast.makeText(this, "Permission denied!", Toast.LENGTH_LONG).show();
                }

                break;
            }
        }
    }
    // Lorsque les résultats sont retournés
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == MY_PERMISSION_REQUEST_CODE_SEND_SMS){
            if (resultCode == RESULT_OK){
                Toast.makeText(this, "Action OK", Toast.LENGTH_LONG).show();
            }else if (resultCode == RESULT_CANCELED){
                Toast.makeText(this, "Action canceled", Toast.LENGTH_LONG).show();
            }else {
                Toast.makeText(this, "Action failed", Toast.LENGTH_LONG).show();
            }
        }
    }
    public boolean onKey(View v, int keyCode, KeyEvent event) {
        if(!phone.getText().toString().isEmpty() && !sms.getText().toString().trim().isEmpty())
            btn.setEnabled(true);
        else
            btn.setEnabled(false);
        return false;
    }
}